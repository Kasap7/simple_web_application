<%@ page session="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
	import="hr.fer.zemris.java.web.servlets.SetColorServlet" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
	String color = (String)session.getAttribute(SetColorServlet.BG_COLOR);
	if (color == null) {
		color = "white";
	}
	
	java.util.Random rnd = new java.util.Random();
	String fontColor = "rgb(" + rnd.nextInt(256) + "," + rnd.nextInt(256) + "," + + rnd.nextInt(256) + ")";
%>

<html>
	<body bgcolor=<%= color %>>
		<h1>Here is a funny story:</h1>
		<div style="color:<%= fontColor %>;">
			A curious child asked his mother: “Mommy, why are some of your hairs turning grey?” <br>
			The mother tried to use this occasion to teach her child: “It is because of you, dear. <br>
			Every bad action of yours will turn one of my hairs grey!” <br>
			The child replied innocently: “Now I know why grandmother has only grey hairs on her head.”
		</div>
	</body>
</html>