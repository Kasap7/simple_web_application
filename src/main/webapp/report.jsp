<%@ page session="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
	import="hr.fer.zemris.java.web.servlets.SetColorServlet" %>

<%
	String color = (String)session.getAttribute(SetColorServlet.BG_COLOR);
	if (color == null) {
		color = "white";
	}
%>

<html>
	<body bgcolor=<%= color %>>
		<h1>OS usage</h1>
		<p>
			Here are the results of OS
			usage in survey that we completed.
		</p>
		<p>
			<img src="reportImage">
	</body>
</html>