<%@ page session="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
	import="hr.fer.zemris.java.web.servlets.SetColorServlet" %>

<%
	String color = (String)session.getAttribute(SetColorServlet.BG_COLOR);
	if (color == null) {
		color = "white";
	}
%>

<html>
	<body bgcolor=<%= color %>>
		<h2>Welcome! Please choose your action:</h2>
		<ul>
			<li><a href="colors.jsp">Background color chooser</a></li>
			<li><a href="trigonometric?a=0&b=90">Trigonometric</a></li>
			<li><a href="stories/funny.jsp">Funny Story</a></li>
			<li><a href="report.jsp">OS survey report</a></li>
			<li><a href="powers?a=1&b=100&n=3">Powers</a></li>
			<li><a href="appinfo.jsp">Application info</a></li>
			<li><a href="glasanje">Vote for your favorite band!</a></li>
		</ul>
	</body>
</html>
