<%@ page session="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
	import="hr.fer.zemris.java.web.servlets.SetColorServlet" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
	String color = (String)session.getAttribute(SetColorServlet.BG_COLOR);
	if (color == null) {
		color = "white";
	}
%>

<html>
	<body bgcolor=<%= color %>>
		<h2>Select background color:</h2>
		<ul>
			<li><a href="setcolor?color=white">WHITE</a></li>
			<li><a href="setcolor?color=red">RED</a></li>
			<li><a href="setcolor?color=green">GREEN</a></li>
			<li><a href="setcolor?color=cyan">CYAN</a></li>
		</ul>
	</body>
</html>