<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" 
	import="hr.fer.zemris.java.web.servlets.SetColorServlet" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
	String color = (String)session.getAttribute(SetColorServlet.BG_COLOR);
	if (color == null) {
		color = "white";
	}
%>

<html>
<head>
     <style>
     table, th, td {
         border: 1px solid black;
         text-align: center
     }
     table {
         width: 50%;
         margin-left: auto; 
         margin-right:auto
     }
    </style>
</head>

<body bgcolor=<%= color %>>
<table>
<caption>Trigonometric</caption>
<tr>
  <th>x</th>
  <th>Sine(x)</th>
  <th>Cosine(x)</th>
</tr>
<c:forEach var="s" items="${sin}" varStatus="status">
<tr>
	<td>${position[status.index]}</td>
    <td>${s}</td>
    <td>${cos[status.index]}</td>
</tr>
</c:forEach>
</table>
</body>
</html>