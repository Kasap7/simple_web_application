<%@ page session="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
	import="hr.fer.zemris.java.web.servlets.SetColorServlet" %>

<%
	String color = (String)session.getAttribute(SetColorServlet.BG_COLOR);
	if (color == null) {
		color = "white";
	}
%>

<html>
	<body bgcolor=<%= color %>>
		<h2>Error: invalid parameters!</h2>
		Cannot make action with given parameters, if you want to return to index.jsp click 
		<a href="index.jsp">here</a>.
	</body>
</html>