<%@ page session="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
	import="hr.fer.zemris.java.web.servlets.SetColorServlet" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
	String color = (String)session.getAttribute(SetColorServlet.BG_COLOR);
	if (color == null) {
		color = "white";
	}
%>

<html>
	<body bgcolor=<%= color %>>
		<h1>Vote for your favorite band:</h1>
		<p>Which one is your favorite? Click on the link to vote!</p>
		<ol>
			<c:forEach var="name" items="${names}" varStatus="status">
				<li><a href="glasanje-glasaj?id=${status.index + 1}">${name}</a></li>
			</c:forEach>
		</ol>
	</body>
</html>