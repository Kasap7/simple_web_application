<%@ page session="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
	import="hr.fer.zemris.java.web.servlets.SetColorServlet" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
	String color = (String)session.getAttribute(SetColorServlet.BG_COLOR);
	if (color == null) {
		color = "white";
	}
%>

<html>
	<head>
		<style type="text/css">
			table.rez td {text-align: center;}
		</style>
	</head>
	<body bgcolor=<%= color %>>
		<h1>Voting results</h1>
		<p>These are voting results.</p>
		
		<table border="1" cellspacing="0" class="rez">
			<thead><tr><th>Band</th><th>Number of votes</th></tr></thead>
			<tbody>
				<c:forEach var="name" items="${names_result}" varStatus="status">
					<tr><td>${name}</td><td>${votes_result[status.index]}</td></tr>
				</c:forEach>
			</tbody>
		</table>
		
		<h2>Graphic results</h2>
		<img alt="Pie-chart" src="glasanje-grafika" width="700" height="400" />
		
		<h2>Results in XLS format</h2>
		<p>Results in XLS format are available <a href="glasanje-xls">here</a></p>
		
		<h2>Other information</h2>
		<p>Song samples from victorious groups:</p>
		
		<ul>
			<c:forEach var="link" items="${links_winners}" varStatus="status">
				<li><a href="${link}" target="_blank">${names_winners[status.index]}</a></li>
			</c:forEach>
		</ul> 
	</body>
</html>