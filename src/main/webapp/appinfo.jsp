<%@ page session="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
	import="hr.fer.zemris.java.web.servlets.SetColorServlet" %>

<%!
private String formatedTime(long timeLeft) {
	long millis = timeLeft % 1000;
	timeLeft /= 1000;
	String millisStr = millis + " milliseconds";
	
	long seconds = timeLeft % 60;
	timeLeft /= 60;
	String secondsStr = seconds + " seconds";
	
	long minutes = timeLeft % 60;
	timeLeft /= 60;
	String minutesStr = minutes + " minutes";
	
	long hours = timeLeft % 24;
	timeLeft /= 24;
	String hoursStr = hours + " hours";
	
	String daysStr = timeLeft + " days";
	
	return String.format(
			"%s, %s, %s, %s and %s",
			daysStr, hoursStr, minutesStr, secondsStr, millisStr
	);
}
%>

<%
	String color = (String)session.getAttribute(SetColorServlet.BG_COLOR);
	if (color == null) {
		color = "white";
	}
	
	long timeStarted = (Long)(session.getServletContext().getAttribute("created_time"));
	long timeLeft = System.currentTimeMillis() - timeStarted;
	String time = formatedTime(timeLeft);
%>

<html>
	<body bgcolor=<%= color %>>
		<h2> How long is server running: </h2>
		<p> <%= time %>
	</body>
</html>