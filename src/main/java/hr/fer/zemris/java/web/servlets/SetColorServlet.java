package hr.fer.zemris.java.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * SetColorServlet is servlet which sets background color attribute of server to one chosen by user.
 * 
 * @author Josip Kasap
 *
 */
public class SetColorServlet extends HttpServlet {
	
	/** Default serial version. */
	private static final long serialVersionUID = 1L;
	
	/** String representation of attribute for server background color. */
	public static final String BG_COLOR = "pickedBgCol";
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		String color = req.getParameter("color");
		if (color == null || color.isEmpty()) return;
		req.getSession().setAttribute(BG_COLOR, color);
		
		req.getRequestDispatcher("index.jsp").forward(req, resp);
	}

}
