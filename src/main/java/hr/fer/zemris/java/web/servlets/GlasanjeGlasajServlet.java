package hr.fer.zemris.java.web.servlets;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.web.glasanje.GlasanjeUtil;

/**
 * GlasanjeGlasajServlet expects single parameter from user: iteger mapped with "id", id represents
 * Band id. This servlet will update given information as a new vote for given band, and will
 * store new results in glasanje-rezultati.txt file. It will also redirect its response to
 * {@link GlasanjeRezultatiServlet}.
 * 
 * @author Josip Kasap
 *
 */
public class GlasanjeGlasajServlet extends HttpServlet {

	/** Default serial version. */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String fileName = req.getSession().getServletContext().getRealPath("/WEB-INF/glasanje-rezultati.txt");
		File file = new File(fileName);
		if (!file.exists()) {
			file.createNewFile();
		}
		
		List<String> lines = Files.readAllLines(file.toPath());
		GlasanjeUtil.updateLines(lines, req.getSession().getServletContext().getRealPath(
				"/WEB-INF/glasanje-definicija.txt"));
		
		String idStr = req.getParameter("id");
		if (idStr == null) {
			req.getRequestDispatcher("/WEB-INF/pages/invalidParameters.jsp").forward(req, resp);
			return;
		}
		
		int id = Integer.parseInt(idStr);
		if (id < 1) {
			return;
		}
		List<String> newLines = new ArrayList<>();
		
		for (String line : lines) {
			if (line.isEmpty()) {
				continue;
			}
			String newLine;
			
			int idLine = Integer.parseInt(line.split("\t")[0]);
			if (idLine == id) {
				String[] splited = line.split("\t");
				int newCount = Integer.parseInt(splited[1]) + 1;
				newLine = splited[0] + "\t" + newCount;
			} else {
				newLine = line;
			}
			
			newLines.add(newLine);
		}
		
		newLines.sort((s1, s2) -> {
			int id1 = Integer.parseInt(s1.split("\t")[0]);
			int id2 = Integer.parseInt(s2.split("\t")[0]);
			int count1 = Integer.parseInt(s1.split("\t")[1]);
			int count2 = Integer.parseInt(s2.split("\t")[1]);
			int r = count2 - count1;
			if (r != 0) return r;
			return id1 - id2;
		});
		
		BufferedWriter writer = new BufferedWriter (new OutputStreamWriter(
				new BufferedOutputStream(new FileOutputStream(file))));
		
		for (String line : newLines) {
			writer.write(line);
			writer.write("\n");
		}
		writer.close();
		
		resp.sendRedirect(req.getContextPath() + "/glasanje-rezultati");
	}
	
}
