package hr.fer.zemris.java.web.listeners;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * TimeListener is simple listener whose only job is to memories current time (in milliseconds)
 * and store that value as servlet context attribute.
 * 
 * @author Josip Kasap
 *
 */
public class TimeListener implements ServletContextListener {
	
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		sce.getServletContext().setAttribute("created_time", System.currentTimeMillis());
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent sce) {}
}
