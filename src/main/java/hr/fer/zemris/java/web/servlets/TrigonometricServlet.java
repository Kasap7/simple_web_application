package hr.fer.zemris.java.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * TrigonometricServlet is servlet used to store results of table of trigonometric functions
 * for integer values given by user. Trigonometric functions used are sine and cosine, angle
 * used is one given by user's parameters "a" and "b". Angles will be values between those
 * 2 values.
 * <p> If "a" is greater than "b", they will be swapped. If "b" exceeds "a" + 720, "b" will
 * be set to "a" + 720. If any argument is missing or is invalid (not integer) they will be set
 * to default values ("a" = 0, "b" = 360).
 * <p> Table will be consisted of all integer values from "a" to "b" (inclusive) and their
 * sine and cosine values.
 * 
 * @author Josip Kasap
 *
 */
public class TrigonometricServlet extends HttpServlet {

	/** Default serial version. */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String aStr = req.getParameter("a");
		String bStr = req.getParameter("b");
		
		if (aStr == null || aStr.isEmpty()) {
			aStr = "0";
		}
		if (bStr == null || bStr.isEmpty()) {
			bStr = "360";
		}
		
		int a, b;
		
		try {
			a = Integer.parseInt(aStr);
		} catch (NumberFormatException ignorable) {
			a = 0;
		}
		try {
			b = Integer.parseInt(bStr);
		} catch (NumberFormatException ignorable) {
			b = 360;
		}
		
		if (a > b) {
			int p = a;
			a = b;
			b = p;
		}
		
		if (b > a + 720) {
			b = a + 720;
		}
		
		int[] position = new int[b - a + 1];
		double[] sin = new double[b - a + 1];
		double[] cos = new double[b - a + 1];
		
		for (int i = 0; i < sin.length; i++) {
			double angle = (a + i) * (Math.PI/180);
			position[i] = a + i;
			sin[i] = round(Math.sin(angle));
			cos[i] = round(Math.cos(angle));
		}
		
		req.setAttribute("sin", sin);
		req.setAttribute("cos", cos);
		req.setAttribute("position", position);
		
		req.getRequestDispatcher("/WEB-INF/pages/trigonometric.jsp").forward(req, resp);
	}
	
	/**
	 * If number is close to integer point this method will round it to integer.
	 * @param x number that is to be rounded
	 * @return rounded number
	 */
	private double round(double x) {
		int sign = x>0?1:-1;
		x = Math.abs(x);
		int intX = (int)x;
		double diff = Math.abs(x - intX);
		
		if (diff < 1E-9) {
			return sign*intX;
		} else if (Math.abs(diff - 1) < 1E-9) {
			return sign*(intX + 1);
		}
		
		return x;
	}
}
