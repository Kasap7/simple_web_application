package hr.fer.zemris.java.web.servlets;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * GlasanjeXlsServlet reads all necessary attributes from request and creates xls file
 * that represents voting results of bands. xls file will be sent to response.
 * 
 * @author Josip Kasap
 *
 */
public class GlasanjeXlsServlet extends HttpServlet {

	/** Default serial version. */
	private static final long serialVersionUID = 1L;
	
	@SuppressWarnings("unchecked")
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<String> names = (List<String>)req.getSession().getAttribute("names_result");
		List<Integer> votes = (List<Integer>)req.getSession().getAttribute("votes_result");
		if (names == null || votes == null || names.size() != votes.size()) return;
		
		HSSFWorkbook hwb = createHWB(names, votes);
		OutputStream out = resp.getOutputStream();
		resp.setContentType("application/vnd.ms-excel");
		hwb.write(out);
		hwb.close();
	}

	/**
	 * Creates HSSFWorkbook from given list of names and votes.
	 * @param names list of names
	 * @param votes list of votes
	 * @return HSSFWorkbook created from given informations
	 */
	private HSSFWorkbook createHWB(List<String> names, List<Integer> votes) {
		HSSFWorkbook hwb = new HSSFWorkbook();
		HSSFSheet sheet =  hwb.createSheet("Band results");
		HSSFRow rowhead = sheet.createRow(0);
		rowhead.createCell(0).setCellValue("Band name");
		rowhead.createCell(1).setCellValue("Votes");
		for (int i = 0, size = names.size(); i < size; i++) {
			HSSFRow row = sheet.createRow(i + 1);
			row.createCell(0).setCellValue(names.get(i));
			row.createCell(1).setCellValue(votes.get(i));
		}
		sheet.autoSizeColumn(0);
		sheet.autoSizeColumn(1);
		return hwb;
	}
}
