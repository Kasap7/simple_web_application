package hr.fer.zemris.java.web.servlets;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * GlasanjeServlet loads glasanje-definicija.txt file and forwards attrubutes from
 * that file to glasanjeIndex.jsp page.
 * 
 * @author Josip Kasap
 *
 */
public class GlasanjeServlet extends HttpServlet {

	/** Default serial version. */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String fileName = req.getSession().getServletContext().getRealPath("/WEB-INF/glasanje-definicija.txt");
		
		List<String> lines = Files.readAllLines(Paths.get(fileName));
		List<String> names = new ArrayList<>();
		List<String> links = new ArrayList<>();
		
		for (String line : lines) {
			String[] splited = line.split("\t");
			names.add(splited[1]);
			links.add(splited[2]);
		}
		
		req.setAttribute("names", names);
		req.setAttribute("links", links);
		
		req.getRequestDispatcher("/WEB-INF/pages/glasanjeIndex.jsp").forward(req, resp);
	}
}