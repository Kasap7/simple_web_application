package hr.fer.zemris.java.web.servlets;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.util.Rotation;

/**
 * ReportImageServlet creates static chart using {@link JFreeChart} library. Chart will
 * be sent to given {@link HttpServletResponse}.
 * 
 * @author Josip Kasap
 *
 */
public class ReportImageServlet extends HttpServlet {

	/** Default serial version. */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PieDataset dataset = createDataset();
		JFreeChart chart = createChart(dataset);
		BufferedImage image = chart.createBufferedImage(700, 400);
		
		resp.setContentType("image/png");
	    OutputStream out = resp.getOutputStream();
	    ImageIO.write(image, "png", out);
	}

	/**
	 * Creates chart with given data-set.
	 * @param dataset data-set of chart
	 * @return new chart from given data-set
	 */
	private JFreeChart createChart(PieDataset dataset) {
		JFreeChart chart = ChartFactory.createPieChart3D(null, dataset, true, true, false);
	    PiePlot3D plot = (PiePlot3D) chart.getPlot();
	    plot.setStartAngle(290);
	    plot.setDirection(Rotation.CLOCKWISE);
	    plot.setForegroundAlpha(0.5f);
	    return chart;
	}

	/**
	 * Creates simple data-set of OS usage.
	 * @return new data-set
	 */
	private PieDataset createDataset() {
		DefaultPieDataset result = new DefaultPieDataset();
        result.setValue("Linux", 29);
        result.setValue("Mac", 20);
        result.setValue("Windows", 51);
        return result;
	}
}
