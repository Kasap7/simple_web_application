package hr.fer.zemris.java.web.servlets;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.web.glasanje.GlasanjeUtil;

/**
 * GlasanjeRezultatiServlet reads results of voting from glasanje-rezultati.txt file, sets all
 * necessary attributes and forwards response to glasanjeRez.jsp page.
 * 
 * @author Josip Kasap
 *
 */
public class GlasanjeRezultatiServlet extends HttpServlet {

	/** Default serial version. */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String fileName = req.getSession().getServletContext().getRealPath("/WEB-INF/glasanje-rezultati.txt");
		String definitionFileName = req.getSession().getServletContext().getRealPath(
				"/WEB-INF/glasanje-definicija.txt");
		
		File file = new File(fileName);
		if (!file.exists()) {
			file.createNewFile();
		}
		
		List<String> definition = Files.readAllLines(Paths.get(definitionFileName));
		List<String> lines = Files.readAllLines(file.toPath());
		GlasanjeUtil.updateLines(lines, definitionFileName);
		List<String> names = new ArrayList<>();
		List<Integer> votes = new ArrayList<>();
		
		for (String line : lines) {
			String[] splited = line.split("\t");
			String name = getName(splited[0], definition);
			int vote = Integer.parseInt(splited[1]);
			names.add(name);
			votes.add(vote);
		}
		
		int maxCount = votes.get(0);
		List<String> victoriousLinks = new ArrayList<>();
		List<String> victoriousNames = new ArrayList<>();
		for (int i = 0, size = votes.size(); i < size; i++) {
			if (votes.get(i) < maxCount) break;
			String[] splited = lines.get(i).split("\t");
			victoriousLinks.add(getLink(splited[0], definition));
			victoriousNames.add(getName(splited[0], definition));
		}
		
		req.getSession().setAttribute("names_result", names);
		req.getSession().setAttribute("votes_result", votes);
		req.setAttribute("links_winners", victoriousLinks);
		req.setAttribute("names_winners", victoriousNames);
		
		req.getRequestDispatcher("/WEB-INF/pages/glasanjeRez.jsp").forward(req, resp);
	}
	
	/**
	 * Returns name from given id and list of lines in glasanje-definicija.txt file.
	 * @param id id of band
	 * @param definition list of lines in glasanje-definicija.txt file
	 * @return name with given id of band
	 */
	private String getName(String id, List<String> definition) {
		for (String line : definition) {
			if (line.startsWith(id)) {
				return line.split("\t")[1];
			}
		}
		return null;
	}
	
	/**
	 * Returns link from given id and list of lines in glasanje-definicija.txt file.
	 * @param id id of band
	 * @param definition list of lines in glasanje-definicija.txt file
	 * @return name with given id of band
	 */
	private String getLink(String id, List<String> definition) {
		for (String line : definition) {
			if (line.startsWith(id)) {
				return line.split("\t")[2];
			}
		}
		return null;
	}
}
