package hr.fer.zemris.java.web.servlets;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * PowersServlet is servlet used to create xml file from parameters given by user.
 * Xml file consists of table of numbers and their powers.
 * <div> This servlet expects 3 arguments: <br>
 * <code>a</code> - starting point of table of numbers, inclusive. <br>
 * <code>b</code> - ending point of table of numbers, inclusive. <br>
 * <code>n</code> - number of sheets in xml file. <br>
 * </div> <p>
 * Created xml file will contain i-th power of number in each row of table. Created
 * table will be sent to given {@link HttpServletResponse}.
 * 
 * @author Josip Kasap
 *
 */
public class PowersServlet extends HttpServlet {

	/** Default serial version. */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String aStr, bStr, nStr;
		aStr = req.getParameter("a");
		bStr = req.getParameter("b");
		nStr = req.getParameter("n");
		
		if (aStr == null || bStr == null || nStr == null) {
			req.getRequestDispatcher("/WEB-INF/pages/invalidParameters.jsp").forward(req, resp);
			return;
		}
		
		int a, b, n;
		try {
			a = Integer.parseInt(aStr);
			b = Integer.parseInt(bStr);
			n = Integer.parseInt(nStr);
		} catch (NumberFormatException ex) {
			req.getRequestDispatcher("/WEB-INF/pages/invalidParameters.jsp").forward(req, resp);
			return;
		}
		
		if (!validArguments(a, b, n)) {
			req.getRequestDispatcher("/WEB-INF/pages/invalidParameters.jsp").forward(req, resp);
			return;
		}
		
		if (a > b) {
			int p = a;
			a = b;
			b = p;
		}
		
		resp.setContentType("application/vnd.ms-excel");
	    OutputStream out = resp.getOutputStream();
	    HSSFWorkbook hwb = new HSSFWorkbook();
	    
		for (int i = 1; i <= n; i++) {
			HSSFSheet sheet =  hwb.createSheet(i + "");
			HSSFRow rowhead = sheet.createRow(0);
			rowhead.createCell(0).setCellValue("Number");
			rowhead.createCell(1).setCellValue("Number ^ " + i);
			for (int j = a; j <= b; j++) {
				HSSFRow row = sheet.createRow(j - a + 1);
				row.createCell(0).setCellValue(j);
				row.createCell(1).setCellValue((int)Math.pow(j, i));
			}
			sheet.autoSizeColumn(0);
			sheet.autoSizeColumn(1);
		}
		
		hwb.write(out);
		hwb.close();
	}

	/**
	 * <div>Checks if arguments are valid:<br>
	 * <code>a</code> - must be between -100 and 100, inclusive<br>
	 * <code>b</code> - must be between -100 and 100, inclusive<br>
	 * <code>n</code> - must be between 1 and 5, inclusive<br>
	 * </div>
	 * @param a starting point
	 * @param b ending point
	 * @param n number of sheets
	 * @return <code>true</code> if arguments are valid and <code>false</code> otherwise
	 */
	private boolean validArguments(int a, int b, int n) {
		if (a < -100 || a > 100) return false;
		if (b < -100 || b > 100) return false;
		if (n < 1 || n > 5) return false;
		return true;
	}
}