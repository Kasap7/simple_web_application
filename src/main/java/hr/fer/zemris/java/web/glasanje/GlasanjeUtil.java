package hr.fer.zemris.java.web.glasanje;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * GlasanjeUtil is utility class used in servlets which work with glasanje-definicija.txt and 
 * glasanje-rezultati.txt files. This class offers methods for retrieval informations from
 * those files.
 * 
 * @author Josip Kasap
 *
 */
public class GlasanjeUtil {
	
	/**
	 * Private constructor to unable instantiating this class.
	 */
	private GlasanjeUtil() {
		super();
	}
	
	/**
	 * This method updates results of survey (represented in list of lines of glasanje-rezultati.txt
	 * file in lines argument) with information from path to glasanje-definicija.txt file given as
	 * String argument. This method will add all bands that given list does not poses as new result
	 * in given list, it will also set votes of new lines to 0.
	 * @param lines list of lines that represent glasanje-rezultati.txt file
	 * @param pathDefintion String representation of path to glasanje-definicija.txt file
	 * @throws IOException if file cannot be opened
	 */
	public static void updateLines(List<String> lines, String pathDefintion) throws IOException {
		if (lines == null || pathDefintion == null) return;
		List<String> definition = Files.readAllLines(Paths.get(pathDefintion));
		if (definition.size() == lines.size()) return;
		
		for (String line : definition) {
			if (line.isEmpty()) continue;
			boolean contains = false;
			String id = line.split("\t")[0];
			for (String resultLine : lines) {
				if (resultLine.startsWith(id)) {
					contains = true;
					break;
				}
			}
			
			if (!contains) {
				lines.add(id + "\t0");
			}
		}
	}
}
