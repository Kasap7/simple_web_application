package hr.fer.zemris.java.web.servlets;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.util.Rotation;

/**
 * GlasanjeGrafikaServlet reads all necessary attributes from request and creates pie chart
 * that represents voting results of bands. Pie chart is represented as png image and is
 * sent to response.
 * 
 * @author Josip Kasap
 *
 */
public class GlasanjeGrafikaServlet extends HttpServlet {

	/** Default serial version. */
	private static final long serialVersionUID = 1L;
	
	@SuppressWarnings("unchecked")
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<String> names = (List<String>)req.getSession().getAttribute("names_result");
		List<Integer> votes = (List<Integer>)req.getSession().getAttribute("votes_result");
		if (names == null || votes == null || names.size() != votes.size()) return;
		
		PieDataset dataset = createDataset(names, votes);
		JFreeChart chart = createChart(dataset);
		BufferedImage image = chart.createBufferedImage(700, 400);
		
		resp.setContentType("image/png");
	    OutputStream out = resp.getOutputStream();
	    ImageIO.write(image, "png", out);
	}
	
	/**
	 * Creates chart from given data-set.
	 * @param dataset given data-set
	 * @return chart from given data-set
	 */
	private JFreeChart createChart(PieDataset dataset) {
		JFreeChart chart = ChartFactory.createPieChart3D(null, dataset, true, true, false);
	    PiePlot3D plot = (PiePlot3D) chart.getPlot();
	    plot.setStartAngle(290);
	    plot.setDirection(Rotation.CLOCKWISE);
	    plot.setForegroundAlpha(0.5f);
	    return chart;
	}

	/**
	 * Creates chart of votes from given list of name and list of votes.
	 * @param names list of names of bands
	 * @param votes list of votes for that band
	 * @return data-set from given informations
	 * @throws IOException if data-set cannot be made
	 */
	private PieDataset createDataset(List<String> names, List<Integer> votes) throws IOException {
		DefaultPieDataset result = new DefaultPieDataset();
		for (int i = 0, size = names.size(); i < size; i++) {
			 result.setValue(names.get(i), votes.get(i));
		}
        return result;
	}
}
